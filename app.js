var restify = require('restify');
var builder = require('botbuilder');
var yandexTranslator = require('yandex.translate');
var yandexSpeech = require('yandex-speech');
var http = require('http');
var data = require('./config.json');
var fs = require('fs');
var request = require("request");

//api.telegram.org/bot
var translator = new yandexTranslator(data.yandexTranslator.token);
var currentLanguage = "en";


var inMemoryStorage = new builder.MemoryBotStorage();
// Возвращает сервер
var server = restify.createServer();

//Поднимает сервер и слушает. Узел разворачивает слушатель listen()
server.listen(process.env.PORT || 3978, function () {
    console.log('%s listening to %s', server.name, server.url);
});

var downloadByURI = function (attachment, address) {
    var message = "";
    request.get(attachment.contentUrl)
        .on('error', function (err) {
            console.error(err);
        })
        .on('response', function (res) {

            console.log(res.statusCode);
            console.log(res.headers['content-type']);
        })
        .pipe(fs.createWriteStream(attachment.name))
        .on('finish', function () {
            yandexSpeech.ASR({
                developer_key: data.yandexSpeech.token,
                file: attachment.name,
            }, function (err, httpResponse, xml) {
                if (err) {
                    message = err;
                    var reply = new builder.Message()
                        .address(address)
                        .text(message);
                    bot.send(reply);
                } else {
                    message = xml;
                    var reply = new builder.Message()
                        .address(address)
                        .text(message);
                    bot.send(reply);
                }
            })
        });

};

var speech = function (attachment) {

    return;
}

//Создает новый экземпляр соединителя чата.
var connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});

// Listen for messages from users 
server.post('/api/messages', connector.listen());

var bot = new builder.UniversalBot(connector);


bot.on('conversationUpdate', function (message) {
    if (message.membersAdded) {
        var name = message.user ? message.user.name : null;
        // Don't respond to bot joining
        if (message.membersAdded[0].name == "Bot") {
            return;
        }
        // This is a system message so need to replace user with actual user
        message.address.user = message.membersAdded[0];

        // Send reply
        var msg = "Привет!";
        var reply = new builder.Message().address(message.address).text(msg);
        bot.send(reply);
    } else if (message.membersRemoved) {
        // See if bot was removed
        var botId = message.address.bot.id;
        for (var i = 0; i < message.membersRemoved.length; i++) {
            if (message.membersRemoved[i].id === botId) {
                // Say goodbye
                var reply = new builder.Message()
                    .address(message.address)
                    .text("Прощай, мой друг.");
                bot.send(reply);
                break;
            }
        }
    }
});


// Первый запуск бота. Знакомство.
bot.dialog('firstRun', function (session) {
    session.userData.firstRun = true;
    session.send("Здравствуйте, меня зовут TranslaterBot." +
            "Я могу перевести ваш текст на любой язык, " +
            "только скажите или напишите, я ваш добрый помощник." +
            "По умолчанию перевожу на английский.")
        .endDialog();
}).triggerAction({
    onFindAction: function (context, callback) {
        // Only trigger if we've never seen user before
        if (!context.userData.firstRun) {
            // Return a score of 1.1 to ensure the first run dialog wins
            callback(null, 1.1);
        } else {
            callback(null, 0.0);
        }
    }
});

//Уведомляет пользователя о том, что бользователь добавил бота в свои контакты
bot.on('contactRelationUpdate', function (message) {
    if (message.action === 'add') {
        var name = message.user ? message.user.name : null;
        var reply = new builder.Message()
            .address(message.address)
            .text("Спасибо, что добавил меня, я тебя не подведу");
        bot.send(reply);
    }
});

bot.dialog('/', function (session) {
    var message = session.message;
    if (message.attachments && message.attachments.length > 0) {
        // Echo back attachment
        var attachment = message.attachments[0];
        downloadByURI(attachment, message.address);

    } else {
        translateMessage(session);
    }

});


bot.dialog("/help", [
    function (session) {

        var msg = new builder.Message(session)
            .textFormat(builder.TextFormat.xml)
            .attachmentLayout(builder.AttachmentLayout.list)
            .attachments([
                new builder.HeroCard(session)
                .title('Выберите язык')
                .subtitle('Текст автоматически будет переведен на нужный вам язык')
                .images([
                    builder.CardImage.create(session, 'http://amblog.ru/wp-content/uploads/2016/12/perevod.jpg')
                ])
                .buttons([
                    builder.CardAction.imBack(session, "English", "English"),
                    builder.CardAction.imBack(session, "Russian", "Russian"),
                    builder.CardAction.imBack(session, "German", "German"),
                    builder.CardAction.imBack(session, "Cancel", "Cancel")


                ])
            ]);
        builder.Prompts.choice(session, msg, "English|Russian|German|Cancel");
    },
    function (session, results) {
        // Use the SDK's built-in ability to pick a response at random.
        switch (results.response.index) {
            case 0:
                currentLanguage = "en";
                break;
            case 1:
                currentLanguage = "ru";
                break;
            case 2:
                currentLanguage = "de";
                break;
            case 3:
                session.endDialog();
                break;
            default:
                session.endDialog();
                break;
        }
        session.endDialog("Ok, your choice is %s", currentLanguage);
    }
]).triggerAction({
    matches: /^help$/i,
});


var translateMessage = function (session) {
    translator.translate(session.message.text, currentLanguage)
        .then(function (data) {
            session.send(data);
        })
        .catch(function (e) {
            session.send(e);
        });
}